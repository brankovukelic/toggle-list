Ext.define('ToggleList.view.ToggleList', {
  extend: 'ToggleList.dataview.CollapsibleList',
  requires: ['ToggleList.dataview.CollapsibleList'],
  xtype: 'toggleList',
  config: {
    title: 'Candidate list',
    itemTpl: '<div class="list-item { lastName }">{ firstName } { lastName } <br> { email }</div>',
    store: 'Users',
    mode: 'MULTI',
    listeners: {
      initialize: 'restoreSelection', 
      select: 'itemSelected',
      deselect: 'itemDeselected',
    },
    headerAttrib: 'data-ln',
    multi: false,
    grouped: true
  },

  /**
   * Experiment with restoring initial selection from store.
   */
  restoreSelection: function() {
    var store = this.getStore();
    var selectedItems = store.queryBy(function(record) {
      return record.get('selected');
    }).getRange();
    if (!Ext.isEmpty(selectedItems)) {
      this.deselectAll(true);
      this.select(selectedItems, true, true);
    }
  },

  /**
   * Experimenting with item selection.
   */
  itemSelected: function(list, record) {
    record.set('selected', true);
    return true;
  },
  itemDeselected: function(list, record) {
    record.set('selected', false);
    return true;
  }
});
