Ext.define('ToggleList.view.Main', {
    extend: 'Ext.navigation.View',
    xtype: 'main',
    requires: [
      'ToggleList.view.ToggleList'
    ],
    config: { 
      items: [
        {xtype: 'toggleList'}
      ]
    }
});
