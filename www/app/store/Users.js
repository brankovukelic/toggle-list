var firstNames = [
  'Tommy',
  'Robby',
  'May',
  'Kim',
  'Jenny',
  'Sallah',
  'Mike'
];
var lastNames = [
  'James',
  'Martinez',
  'Donovan',
  'Griffith',
  'Johnas',
  'Terry'
];
var selected = [true, false];

var randomChoice = function(arr) {
  return arr[~~(Math.random() * arr.length)];
};

var data = [];
var i;
var name;
var last;
var email;

for (i = 40; i--;) {
  name = randomChoice(firstNames);
  last = randomChoice(lastNames);
  email = name.toLowerCase() + '.' + last.toLowerCase() + '@example.com';
  data.push({
    id: i,
    firstName: name,
    lastName: last,
    email: email,
    selected: randomChoice(selected)
  });
}

Ext.define('ToggleList.store.Users', {
  extend: 'Ext.data.Store',
  xtype: 'users',
  config: {
    model: 'ToggleList.model.User',
    sorters: ['lastName', 'firstName', 'id'],
    grouper: {
      groupFn: function(item) {
        return '<div class="header" data-ln="' + item.get('lastName') + '">' + item.get('lastName') + '</div>';
      }
    },
    data: data
  },
});
