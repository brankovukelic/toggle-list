Ext.define('ToggleList.dataview.CollapsibleList', {
  extend: 'Ext.List',
  alias: 'dataview.CollapsibleList',
  xtype: 'collapsiblelist',

  /**
   * Configuration options
   */
  config: {
    listeners: {
      painted: 'setUpInitialCollapsedState',
      tap: {
        element: 'element',
        delegate: 'div.x-list-header',
        fn: 'headerTapped'
      }
    },
    /**
     * `headerCls`
     *
     * The class attribute of the hader element. 
     *
     * This class should be applied to the clickable portion of the header
     * element.
     *
     * Default is 'header'.
     *
     */
    headerCls: 'header',

    /**
     * `collapsedCls`
     *  
     * The class applied to clickable portion of the group header when group is
     * collapsed.
     *
     * Default is 'collapsed'.
     *
     */
    collapsedCls: 'collapsed',

    /**
     * `headerAttrib`
     *
     * The attrbiute that contains the class that is applied to all items in
     * the group.
     *
     * Default is 'data-group'.
     *
     */
    headerAttrib: 'data-group',

    /**
     * `listItemCls`
     *
     * The class attribute of list item contents.
     *
     * This class is used to locate all group items when `#collapseAll()` is
     * called.
     *
     * Default is 'list-item'.
     *
     */
    listItemCls: 'list-item',

    /**
     * `multi`
     *
     * This option is a boolean that allows more than one group to be shown.
     *
     * Default is `true`.
     *
     */
    multi: true,

    /**
     * `canCollapseShown`
     *
     * This option is a boolean that allows the shown group to be collapsed
     * when set to `true`.
     *
     * Default is `false`.
     *
     */
    canCollapseShown: false,

    /**
     * `startCollapsed`
     *
     * This option is a boolean that makes the list start collapsed when set to
     * `true`.
     *
     * Default is `true`.
     *
     */
    startCollapsed: true,

    /**
     * `showFirstInitially`
     *
     * This option is a boolean that uncollapses the first item initially when
     * set to `true`. This is only useful when `startCollapsed` is also `true`.
     *
     * Default is `true`.
     *
     */
    showFirstInitially: true,
  },

  /**
   * `#getGroupItems(header)`
   */
  getGroupItems: function(header) {
    var lastName = header.getAttribute(this.getHeaderAttrib());
    return Ext.select('div.' + lastName);
  },

  /**
   * `#forEachGroupItem(groupItems, fn)`
   *
   * Execute `fn` callback function on array of elements from a group.
   *
   * `groupItems` should be a `Ext.dom.CompositeElement` instance or an object
   * that has an array of DOM nodes in its `elements` property.
   *
   * The callback function will take a single `Ext.dom.Element` instance as its
   * only argument.
   *
   */
  forEachGroupItem: function(groupItems, fn) {
    Ext.Array.each(groupItems.elements, function(element) {
      var item = Ext.get(element).findParentNode('.x-list-item', null, true);
      fn(item);
    });
  },

  /**
   * `#collapseGroup(header)`
   *
   * Collapses a group of items related to a `header`.
   *
   */
  collapseGroup: function(header) {
    var groupItems = this.getGroupItems(header);
    this.forEachGroupItem(groupItems, function(item) {
      item.hide();
    });
    header.addCls(this.getCollapsedCls());
  },

  /**
   * `#showGroup(header)`
   *
   * Show all group items related to a `header`.
   *
   */
  showGroup: function(header) {
    var groupItems = this.getGroupItems(header);
    this.forEachGroupItem(groupItems, function(item) {
      item.show();
    });
    header.removeCls(this.getCollapsedCls());
  },

  /**
   * `#collapseAll()`
   *
   * Collapse all groups.
   *
   */
  collapseAll: function() {
    var me = this;
    var element = this.element;
    var groupHeaders = element.query('.' + this.getHeaderCls());
    var groupItems = Ext.select(element.query('.' + this.getListItemCls()));
    Ext.Array.each(groupHeaders, function(header) {
      header = Ext.get(header);
      header.addCls(me.getCollapsedCls());
    });
    this.forEachGroupItem(groupItems, function(item) {
      item.hide();
    });
  },

  /**
   * `#setUpInitialCollapsedState()`
   *
   * Sets up the initial collapsed state of all groups depending on
   * `startCollapsed` and `showFirstInitailly` options.
   *
   */
  setUpInitialCollapsedState: function() {
    var firstHeader;

    if (this.getStartCollapsed()) {
      this.collapseAll();
    }
    if (this.getShowFirstInitially()) {
      firstHeader = Ext.get(this.element.query('.' + this.getHeaderCls())[0]);
      this.showGroup(firstHeader);
    }
  },

  /**
   * `#headerTapped(e, target)`
   *
   * Tap event handler for group header. This method will locate all list items
   * related to this header and collapse them.
   *
   * This method makes a few assumptions that are important for constructing
   * relevant header and item templates. First, the header must have a
   * `data-ln` attribute that matches the last name (which is used for
   * grouping). The value of the `data-ln` attribute must match one of the
   * classes of the items in the group. So, for example:
   *
   *     <div class="header" data-ln="James">James</div>
   *
   * will match all items that have a class of "James".
   *
   * It also assumes that all items with the given class have a parent which
   * has a class of '.x-list-item'.
   *
   * The `<div>` containing the header will receive a class of 'collapsed' when
   * all related items are hidden. When the items are shown, the 'collapsed'
   * class is removed.
   *
   */
  headerTapped: function(e, target) {
    var header = Ext.get(target);
    var isCollapsed = header.hasCls(this.getCollapsedCls());
    if (isCollapsed) {
      if (!this.getMulti()) {
        this.collapseAll();
      }
      this.showGroup(header);
    } else {
      if (this.getCanCollapseShown()) {
        this.collapseGroup(header);
      }
    }
  },

  /**
   * Overload `handlePinnedHeader` so that group header is not pinned at the
   * top on scroll. This method usually recalculates the header position to
   * make it stick to the top of the list. We overload it with an empty
   * function so that it has no effect on group header position.
   */
  handlePinnedHeader: function() { }
});
