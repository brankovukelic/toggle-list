Ext.define('ToggleList.model.User', {
  extend: 'Ext.data.Model',
  config: {
    fields: [
      {name: 'id', type: 'int'},
      {name: 'firstName', type: 'strnig'},
      {name: 'lastName', type: 'string'},
      {name: 'email', type: 'string'},
      {name: 'selected', type: 'boolean'}
    ]
  }
});
